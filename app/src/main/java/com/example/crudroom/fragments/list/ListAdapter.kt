package com.example.crudroom.fragments.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.crudroom.R
import com.example.crudroom.modal.User
import kotlinx.android.synthetic.main.custom_row.view.*

class ListAdapter1 : RecyclerView.Adapter<ListAdapter1.MyViewHolder>() {

    private var userList = emptyList<User>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.custom_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var currentItem = userList[position]
        holder.itemView.txt_id.text = currentItem.id.toString()
        holder.itemView.txt_firstname.text = currentItem.firstName
        holder.itemView.txt_lastname.text = currentItem.lastName
        holder.itemView.txt_age.text = currentItem.age.toString()

        holder.itemView.rowLayout.setOnClickListener {
            //val action = ListFragmentDirection.actionListFragmentToUpdateFragment(currentItem)
            // holder.itemView.findNavController().navigate(action)
        }
    }

    override fun getItemCount(): Int {

        return userList.size

    }

    fun setData(user: List<User>) {
        this.userList = user
        notifyDataSetChanged()
    }


}