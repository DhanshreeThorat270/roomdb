package com.example.crudroom.repository

import androidx.lifecycle.LiveData
import com.example.crudroom.data.UserDao
import com.example.crudroom.modal.User

class UserRepository(private val userDao: UserDao) {

    val readAllData: LiveData<List<User>> = userDao.readAllData()

    suspend fun addUser(user: User) {
        userDao.addUser(user)
    }

    suspend fun updateUser(user: User) {
        userDao.updateUser(user)
    }

    suspend fun deleteUser(user: User) {
        userDao.deleteUser(user)
    }

    fun deleteAllUsers() {
        userDao.deleteAllUsers()
    }

}